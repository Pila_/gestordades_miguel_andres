package cat.itb.migueliandres.dam.m03.uf2.project.tests;

import cat.itb.migueliandres.dam.m03.uf2.project.data.Director;
import cat.itb.migueliandres.dam.m03.uf2.project.data.Movie;
import org.junit.Test;

import static org.junit.jupiter.api.Assertions.*;

public class MovieTest {
    Director director = new Director("George Roy", "Hill");
    Movie movie1 = new Movie("The Sting", 1973, director, "Comedia", 100, 10);

    @Test
    public void getDirector(){
        assertEquals(movie1.director, movie1.getDirector());
    }
}