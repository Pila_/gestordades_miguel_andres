package cat.itb.migueliandres.dam.m03.uf2.project.data;

import java.util.List;
import java.util.Scanner;

public class DirectorStats {

    public static String countMoviesForDirector(MovieDatabase moviesAndDirectors, Scanner scanner) {

        //Get both movies and directors lists from moviesAndDirectors
        List<Movie> movies = moviesAndDirectors.movies;
        List<Director> directors = moviesAndDirectors.directors;

        //The user selects a director
        Director director = Director.readDirector(scanner);

        //Selected director
        String directorName = director.name;
        String directorSurname = director.surname;

        //Count movies for selected director
        int numberOfMovies = 0;
        for(int i=0; i<movies.size(); i++){
            for (int j=0; j<directors.size(); j++) {
                //Get both directors' (the one in the list, and the one the user selected) names and surnames
                Director directorMovies = movies.get(i).director;

                //Movies list director
                String directorMoviesName = directorMovies.name;
                String directorMoviesSurname = directorMovies.surname;

                if (directorMoviesName.equals(directorName) && directorMoviesSurname.equals(directorSurname)) {
                    numberOfMovies++;
                }
            }
        }
        return String.format("%s %s ha dirigit %d pel·lícules", directorName, directorSurname, numberOfMovies);
    }

    public static String averageBenefit(MovieDatabase moviesAndDirectors, Scanner scanner) {
        //Get the movie list from moviesAndDirectors
        List<Movie> movies = moviesAndDirectors.movies;

        //The user selects a director
        Director director = Director.readDirector(scanner);

        //Selected director
        String directorName = director.name;
        String directorSurname = director.surname;

        //Add the benefits of the selected director, and count their movies
        int totalBenefits = 0;
        int countMoviesForDirector = 0;
        for(int i=0; i<movies.size(); i++) {
            //Get both directors' (the one in the list, and the one the user selected) names and surnames
            Director directorMovies = movies.get(i).director;

            //Movies list director
            String directorMoviesName = directorMovies.name;
            String directorMoviesSurname = directorMovies.surname;

            if (directorMoviesName.equals(directorName) && directorMoviesSurname.equals(directorSurname)) {
                int benefit = (movies.get(i).revenue) - (movies.get(i).budget);
                totalBenefits = totalBenefits + benefit;
                countMoviesForDirector++;
            }
        }

        //Calculate the average benefit and return it
        int averageBenefit = totalBenefits/countMoviesForDirector;
        return String.format("La mitjana de beneficis de %s %s és %d", directorName, directorSurname, averageBenefit);
    }

}
