package cat.itb.migueliandres.dam.m03.uf2.project.ui;

import cat.itb.migueliandres.dam.m03.uf2.project.data.Director;
import cat.itb.migueliandres.dam.m03.uf2.project.data.Movie;
import cat.itb.migueliandres.dam.m03.uf2.project.data.MovieDatabase;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        List<Director> directors = new ArrayList<>();
        List<Movie> movies = new ArrayList<>();
        MovieDatabase moviesAndDirectors = new MovieDatabase(directors, movies);
        Stats.runMainMenu(scanner, movies, directors, moviesAndDirectors);
    }
}
