package cat.itb.migueliandres.dam.m03.uf2.project.tests;

import cat.itb.migueliandres.dam.m03.uf2.project.data.Director;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class DirectorTest {
    Director dir = new Director("Paco", "Jones");

    @Test
    public void getNomTest() {
        assertEquals("Paco", dir.getName());
        assertEquals("Jones", dir.getSurname());
    }
}

