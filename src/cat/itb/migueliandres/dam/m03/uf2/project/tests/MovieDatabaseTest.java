package cat.itb.migueliandres.dam.m03.uf2.project.tests;


import cat.itb.migueliandres.dam.m03.uf2.project.data.Director;
import cat.itb.migueliandres.dam.m03.uf2.project.data.Movie;
import cat.itb.migueliandres.dam.m03.uf2.project.data.MovieDatabase;
import org.junit.Test;
import static org.junit.Assert.assertEquals;

import java.util.ArrayList;
import java.util.List;

public class MovieDatabaseTest {
    Director director = new Director("George Roy", "Hill");
    Director director1 = new Director("Quentin", "Tarantino");
    Movie movie1 = new Movie("The Sting", 1973, director, "Comedia", 100, 10);
    Movie movie2 = new Movie("Pulp Fiction", 1994, director1, "Crim", 200, 30);

    @Test
    public void getMovieDataBase() {
        List<Director> directors = new ArrayList<>();
        List<Movie> movies = new ArrayList<>();
        directors.add(director);
        directors.add(director1);
        movies.add(movie1);
        movies.add(movie2);
        MovieDatabase moviesAndDirectors = new MovieDatabase(directors, movies);
        assertEquals(movies, moviesAndDirectors.getMovies());
    }

}