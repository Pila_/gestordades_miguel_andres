package cat.itb.migueliandres.dam.m03.uf2.project.ui;

import cat.itb.migueliandres.dam.m03.uf2.project.data.*;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class Stats {

    public static int selectMainMenuOption(Scanner scanner){
        int selectedOption;
        PrintMenu.printMainMenu();
        selectedOption = scanner.nextInt();
        scanner.nextLine();
        return selectedOption;
    }

    public static void runMainMenu(Scanner scanner, List<Movie> movies, List<Director> directors, MovieDatabase moviesAndDirectors){
        //The user selects the option
        int selectedOption = selectMainMenuOption(scanner);
        //Option 0: exit program
        while (selectedOption != 0) {
            switch (selectedOption) {
                case 1:
                    //Add movies to movies list, if option selected (selectedOption) equals 0, return to main menu
                    movies.add(Movie.readMovie(scanner, directors));
                    selectedOption = scanner.nextInt();
                    while (selectedOption!=0) {
                        scanner.nextLine();
                        movies.add(Movie.readMovie(scanner, directors));
                        selectedOption = scanner.nextInt();
                    }
                    break;
                case 2:
                    //Print stats menu and declare a new int to store the option selected by the user.
                    //Until the user asks to return, they will stay in the same menu
                    PrintMenu.printMovieStatsMenu();
                    selectedOption = scanner.nextInt();
                    scanner.nextLine();
                    while (selectedOption!=0) {
                        selectMovieOperation(selectedOption, movies);
                        selectedOption = scanner.nextInt();
                        scanner.nextLine();
                    }
                    break;
                case 3:
                    //Print stats menu and declare a new int to store the option selected by the user.
                    //Until the user asks to return, they will stay in the same menu
                    PrintMenu.printDirectorStatsMenu();
                    selectedOption = scanner.nextInt();
                    scanner.nextLine();
                    while (selectedOption != 0) {
                        selectDirectorOperation(selectedOption, moviesAndDirectors, scanner);
                        selectedOption = scanner.nextInt();
                        scanner.nextLine();
                    }
                    break;
                case 0:
                    //Option 0 returns to main menu
                    break;
            }
            //Print Main menu again and read the option selected by user
            selectedOption=selectMainMenuOption(scanner);
        }

    }

    private static void selectMovieOperation(int selectedOption, List<Movie> movies) {
        switch (selectedOption){
            case 1:
                //Prints the oldest movie from the movies list
                System.out.println(MoviesStats.oldestMovie(movies));
                break;
            case 2:
                //Prints the newest movie from the movies list
                System.out.println(MoviesStats.newestMovie(movies));
                break;
            case 3:
                //Prints the movie with highest budget from the movies list
                System.out.println(MoviesStats.highestBudget(movies));
                break;
            case 4:
                //Prints the movie with lowest budget from the movies list
                System.out.println(MoviesStats.lowestBudget(movies));
                break;
            case 5:
                //Prints the movie with highest revenue from the movies list
                System.out.println(MoviesStats.highestRevenue(movies));
                break;
            case 6:
                //Prints the movie with lowest revenue from the movies list
                System.out.println(MoviesStats.lowestRevenue(movies));
                break;
            case 7:
                //Prints the movie with highest benefit from the movies list
                System.out.println(MoviesStats.highestBenefit(movies));
                break;
            case 8:
                //Prints the movie with lowest benefit from the movies list
                System.out.println(MoviesStats.lowestBenefit(movies));
                break;
            case 0:
                //Returns to main menu
                break;
        }
        PrintMenu.printMovieStatsMenu();
    }

    private static void selectDirectorOperation(int selectedOption, MovieDatabase moviesAndDirectors, Scanner scanner) {
        switch (selectedOption){
            case 1:
                //Counts how many movies has a director directed
                System.out.println(DirectorStats.countMoviesForDirector(moviesAndDirectors, scanner));
                break;
            case 2:
                //Calculates the average benefits of the movies a director has directed
                System.out.println(DirectorStats.averageBenefit(moviesAndDirectors, scanner));
                break;
            case 0:
                //Returns to main menu
                break;
        }
        PrintMenu.printDirectorStatsMenu();
    }


}
