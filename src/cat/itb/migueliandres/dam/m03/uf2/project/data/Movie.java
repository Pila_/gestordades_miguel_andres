package cat.itb.migueliandres.dam.m03.uf2.project.data;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class Movie {
    String genre;
    public Director director;
    String title;
    int revenue;
    int budget;
    int year;

    public Movie(String title, int year, Director director, String genre, int revenue, int budget) {
        this.genre = genre;
        this.director = director;
        this.title = title;
        this.revenue = revenue;
        this.budget = budget;
        this.year = year;
    }

    public String getGenre() {
        return genre;
    }

    public Director getDirector() {
        return director;
    }

    public String getTitle() {
        return title;
    }

    public int getRevenue() {
        return revenue;
    }

    public int getBudget() {
        return budget;
    }

    public int getYear() {
        return year;
    }

    public static Movie readMovie(Scanner scanner, List<Director> directors) {

        System.out.println("1. Introdueix el títol:");
        String title = scanner.nextLine();

        System.out.println("2. Introdueix l'any:");
        int year = scanner.nextInt();
        scanner.nextLine();

        System.out.println("3. Introdueix el director:");
        Director director = Director.readDirector(scanner);
        for (int i = 0; i<directors.size(); i++) {
            if (!director.equals(directors.get(i))) {
                directors.add(director);
            }
        }

        System.out.println("4. Introdueix el gènere:");
        String genre = scanner.nextLine();

        System.out.println("5. Introdueix la recaudació ($):");
        int revenue = scanner.nextInt();
        scanner.nextLine();

        System.out.println("6. Introdueix el presupost ($):");
        int budget = scanner.nextInt();
        scanner.nextLine();

        System.out.println("Si has finalitzar l'entrada de pel·lícules, introdueix '0'.\n"+"Si no és així, introdueix qualsevol altre número.");

        Movie movie = new Movie(title, year, director, genre, revenue, budget);

        return movie;
    }
}