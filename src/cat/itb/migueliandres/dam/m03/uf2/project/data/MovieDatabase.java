package cat.itb.migueliandres.dam.m03.uf2.project.data;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class MovieDatabase {
    List<Director> directors;
    List<Movie> movies;

    public MovieDatabase(List<Director> director, List<Movie> movies) {
        this.directors = director;
        this.movies = movies;
    }

    public List<Director> getDirector() {
        return directors;
    }

    public List<Movie> getMovies() {
        return movies;
    }

    public static List<Movie> moviesListRead(Scanner scanner, List<Director> directors) {
        System.out.println("Quantes pel·lícules vols introduir?");
        int amountOfMovies = scanner.nextInt();
        scanner.nextLine();
        List<Movie> moviesList = new ArrayList<>();
        for (int i=0; i<amountOfMovies; i++){
            moviesList.add(Movie.readMovie(scanner, directors));
        }

        return moviesList;
    }

    public static List<Director> directorListRead(List<Movie> movies){
        ArrayList<Director> directorList = new ArrayList<>();
        for (int i = 0; i<movies.size(); i++){
            directorList.add(movies.get(i).director);
        }
        return directorList;
    }

    public static String age(Movie movie) {
        return String.format("%s, dirigida per %s, té %d anys.",movie.title, movie.director.toString(), 2021-movie.year);
    }

    public static String benefit(Movie movie) {
        if(movie.revenue-movie.budget>=0){
            return String.format("%s, dirigida per %s, va obtindre %d€ de beneficis.",movie.title, movie.director.toString(), movie.revenue-movie.budget);
        }else{
            return String.format("%s, dirigida per %s, va registrar %d€ de pèrdues.",movie.title, movie.director.toString(), movie.revenue-movie.budget);
        }
    }
}