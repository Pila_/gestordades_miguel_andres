package cat.itb.migueliandres.dam.m03.uf2.project.ui;

public class PrintMenu {
    public static void printMainMenu(){
        System.out.println("*~/\\~* >MovieApp< *~/\\~*");
        System.out.println();
        System.out.println("Quina operació vol realitzar?");
        System.out.println("1) Introduir dades de Película.");
        System.out.println("2) Estadístiques de Película.");
        System.out.println("3) Estadístiques de Director.");
        System.out.println("0) Sortir.");
    }
    public static void printMovieStatsMenu(){
        System.out.println("~*~ Estadístiques de Pel·lícula ~*~");
        System.out.println();
        System.out.println("Quina operació vols realitzar?");
        System.out.println("1. Mostra la pel·lícula més antiga.");
        System.out.println("2. Mostra la pel·lícula més recent.");
        System.out.println("3. Mostra la pel·lícula amb més pressupost.");
        System.out.println("4. Mostra la pel·lícula amb menys pressupost.");
        System.out.println("5. Mostra la pel·lícula amb major recaudació.");
        System.out.println("6. Mostra la pel·lícula amb menor recaudació.");
        System.out.println("7. Mostra la pel·lícula amb més beneficis.");
        System.out.println("8. Mostra la pel·lícula amb menys beneficis.");
        System.out.println("0. Enrere");
    }
    public static void printDirectorStatsMenu(){
        System.out.println("~*~ Estadístiques de Director ~*~");
        System.out.println("Quina operació vols realitzar?");
        System.out.println("1) Mostra el total de películes del director.");
        System.out.println("2) Mostra la mitja de beneficis per pel·lícula");
        System.out.println("0) Enrere");
    }
}
