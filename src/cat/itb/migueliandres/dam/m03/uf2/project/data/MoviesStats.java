package cat.itb.migueliandres.dam.m03.uf2.project.data;

import java.util.List;

public class MoviesStats {

    public static String oldestMovie(List<Movie> movies) {
        int year = movies.get(0).year;
        String title = movies.get(0).title;
        String directorName = movies.get(0).director.name;
        String directorSurname = movies.get(0).director.surname;

        for (int i=0; i<movies.size(); i++){
            if (year>movies.get(i).year){
                year = movies.get(i).year;
                title = movies.get(i).title;
                directorName = movies.get(i).director.name;
                directorSurname = movies.get(i).director.surname;
            }
        }
        return String.format("La pel·lícula més antiga és %s (%d), dirigida per %s %s.",title,year,directorName, directorSurname);
    }

    public static String newestMovie(List<Movie> movies) {
        int year = movies.get(0).year;
        String title = movies.get(0).title;
        String directorName = movies.get(0).director.name;
        String directorSurname = movies.get(0).director.surname;

        for (int i=0; i<movies.size(); i++){
            if (year>movies.get(i).year){
                year = movies.get(i).year;
                title = movies.get(i).title;
                directorName = movies.get(i).director.name;
                directorSurname = movies.get(i).director.surname;
            }
        }
        return String.format("La pel·lícula més nova és %s (%d), dirigida per %s %s.",title,year,directorName, directorSurname);

    }

    public static String lowestBudget(List<Movie> movies) {
        int minimumBudget = movies.get(0).budget;
        int year = movies.get(0).year;
        String title = movies.get(0).title;
        String directorName = movies.get(0).director.name;
        String directorSurname = movies.get(0).director.surname;

        for (int i=0; i<movies.size(); i++){
            if (minimumBudget>movies.get(i).budget){
                minimumBudget = movies.get(i).budget;
                year = movies.get(i).year;
                title = movies.get(i).title;
                directorName = movies.get(i).director.name;
                directorSurname = movies.get(i).director.surname;
            }
        }
        return String.format("La pel·lícula amb menys presupost és %s (%d), dirigida per %s %s (%d€).",title,year,directorName, directorSurname,minimumBudget);
    }

    public static String highestBudget(List<Movie> movies) {
        int maximumBudget = movies.get(0).budget;
        int year = movies.get(0).year;
        String title = movies.get(0).title;
        String directorName = movies.get(0).director.name;
        String directorSurname = movies.get(0).director.surname;

        for (int i=0; i<movies.size(); i++){
            if (maximumBudget<movies.get(i).budget){
                maximumBudget = movies.get(i).budget;
                year = movies.get(i).year;
                title = movies.get(i).title;
                directorName = movies.get(i).director.name;
                directorSurname = movies.get(i).director.surname;
            }
        }
        return String.format("La pel·lícula amb més presupost és %s (%d), dirigida per %s %s (%d€).",title,year,directorName,directorSurname,maximumBudget);
    }

    public static String lowestRevenue(List<Movie> movies) {
        int lowestRevenue = movies.get(0).revenue;
        int year = movies.get(0).year;
        String title = movies.get(0).title;
        String directorName = movies.get(0).director.name;
        String directorSurname = movies.get(0).director.surname;

        for(int i = 0; i<movies.size(); i++){
            if(lowestRevenue>movies.get(i).revenue){
                lowestRevenue=movies.get(i).revenue;
                year = movies.get(i).year;
                title = movies.get(i).title;
                directorName = movies.get(i).director.name;
                directorSurname = movies.get(i).director.surname;
            }
        }
        return String.format("La pel·lícula amb menys recaudacions és %s (%d), dirigida per %s %s (%d€).",title,year,directorName, directorSurname,lowestRevenue);
    }

    public static String highestRevenue(List<Movie> movies) {
        int highestRevenue = movies.get(0).revenue;
        int year = movies.get(0).year;
        String title = movies.get(0).title;
        String directorName = movies.get(0).director.name;
        String directorSurname = movies.get(0).director.surname;

        for(int i=0; i<movies.size(); i++){
            if(highestRevenue<movies.get(i).revenue){
                highestRevenue=movies.get(i).revenue;
                year = movies.get(i).year;
                title = movies.get(i).title;
                directorName = movies.get(i).director.name;
                directorSurname = movies.get(i).director.surname;
            }
        }
        return String.format("La pel·lícula amb més recaudacions és %s (%d), dirigida per %s %s (%d€).",title,year,directorName, directorSurname,highestRevenue);
    }

    public static String lowestBenefit(List<Movie> movies) {
        int lowestBenefit = (movies.get(0).revenue)-(movies.get(0).budget);
        int year = movies.get(0).year;
        String title = movies.get(0).title;
        String directorName = movies.get(0).director.name;
        String directorSurname = movies.get(0).director.surname;

        for(int i=0; i<movies.size(); i++){
            int beneficios = (movies.get(i).revenue)-(movies.get(i).budget);
            if(lowestBenefit>beneficios){
                lowestBenefit = beneficios;
                year = movies.get(i).year;
                title = movies.get(i).title;
                directorName = movies.get(i).director.name;
                directorSurname = movies.get(i).director.surname;
            }
        }
        return String.format("La pel·lícula amb menys beneficis és %s (%d), dirigida per %s %s (%d€).",title,year,directorName, directorSurname, lowestBenefit);
    }

    public static String highestBenefit(List<Movie> movies) {
        int highestBenefit = (movies.get(0).revenue)-(movies.get(0).budget);
        int year = movies.get(0).year;
        String title = movies.get(0).title;
        String directorName = movies.get(0).director.name;
        String directorSurname = movies.get(0).director.surname;

        for(int i=0; i<movies.size(); i++){
            int beneficios = (movies.get(i).revenue)-(movies.get(i).budget);
            if(highestBenefit<beneficios){
                highestBenefit = beneficios;
                year = movies.get(i).year;
                title = movies.get(i).title;
                directorName = movies.get(i).director.name;
                directorSurname = movies.get(i).director.surname;
            }
        }
        return String.format("La pel·lícula amb més beneficis és %s (%d), dirigida per %s %s (%d€).",title,year,directorName, directorSurname,highestBenefit);
    }
}