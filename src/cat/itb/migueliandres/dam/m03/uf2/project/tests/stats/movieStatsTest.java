package cat.itb.migueliandres.dam.m03.uf2.project.tests.stats;

import cat.itb.migueliandres.dam.m03.uf2.project.data.Director;
import cat.itb.migueliandres.dam.m03.uf2.project.data.Movie;
import cat.itb.migueliandres.dam.m03.uf2.project.data.MoviesStats;
import org.junit.Test;
import static org.junit.Assert.assertEquals;

import java.util.ArrayList;
import java.util.List;

public class movieStatsTest {
    Director director = new Director("George Roy", "Hill");
    Director director1 = new Director("Quentin", "Tarantino");
    Movie movie1 = new Movie("The Sting", 1973, director, "Comedia", 100, 10);
    Movie movie2 = new Movie("Pulp Fiction", 1994, director1, "Crim", 200, 30);
    List<Movie> movies = new ArrayList<>();
    @Test
    public void getOldestMovie(){
        movies.add(movie1);
        movies.add(movie2);
        String result = String.format("La pel·lícula més antiga és %s (%d), dirigida per %s %s.",movie1.getTitle(),movie1.getYear(),director.getName(), director.getSurname());
        assertEquals(result, MoviesStats.oldestMovie(movies));
    }
    @Test
    public void getLowestBudget(){
        movies.add(movie1);
        movies.add(movie2);
        String result = String.format("La pel·lícula amb menys presupost és %s (%d), dirigida per %s %s (%d€).",movie1.getTitle(),movie1.getYear(),director.getName(), director.getSurname(), movie1.getBudget());
        assertEquals(result, MoviesStats.lowestBudget(movies));
    }

    @Test
    public void getLowestRevenue(){
        movies.add(movie1);
        movies.add(movie2);
        String result = String.format("La pel·lícula amb menys recaudacions és %s (%d), dirigida per %s %s (%d€).",movie1.getTitle(),movie1.getYear(),director.getName(), director.getSurname(), movie1.getRevenue());
        assertEquals(result, MoviesStats.lowestRevenue(movies));
    }

    @Test
    public void getHighestBenefit(){
        movies.add(movie1);
        movies.add(movie2);
        int highestBenefit = movie2.getRevenue() - movie2.getBudget();
        String result = String.format("La pel·lícula amb més beneficis és %s (%d), dirigida per %s %s (%d€).",movie2.getTitle(),movie2.getYear(),director1.getName(), director1.getSurname(),highestBenefit);;
        assertEquals(result, MoviesStats.highestBenefit(movies));
    }
}
