package cat.itb.migueliandres.dam.m03.uf2.project.data;

import javax.swing.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class Director {
    String name;
    String surname;

    public Director(String name, String surname) {
        this.name = name;
        this.surname = surname;
    }

    public String getName() { return name; }

    public String getSurname() {
        return surname;
    }

    public static Director readDirector(Scanner scanner) {
        System.out.println("   Introdueix el nom del director:");
        String name = scanner.nextLine();
        System.out.println("   Introdueix el cognom del director:");
        String surname = scanner.nextLine();
        Director director = new Director(name, surname);

        return director;
    }
}
